# Project Sungai


[![pipeline status](https://gitlab.com/antaraksi/project-sungai/badges/master/pipeline.svg)](https://gitlab.com/antaraksi/project-sungai/commits/master)


Simple Online Shop rest api with Lumen Framework.

The Project Sungai is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
