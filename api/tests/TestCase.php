<?php

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    /**
     * Convenience method for creating a shop
     *
     * @param int $count
     * @return mixed
     */
    protected function shopFactory($count = 1)
    {
        $shops = factory(\App\Shop::class, $count)->make();

        foreach ($shops as $shop) {
            $shop->save();
        }

        return $shops;
    }
}
