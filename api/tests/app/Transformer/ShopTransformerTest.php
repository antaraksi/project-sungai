<?php

namespace Tests\App\Transformer;

use TestCase;
use App\Shop;
use App\Transformer\ShopTransformer;
use League\Fractal\TransformerAbstract;
use Laravel\Lumen\Testing\DatabaseMigrations;

class ShopTransformerTest extends TestCase
{
    use DatabaseMigrations;

    /** @test **/
    public function it_can_be_initialized()
    {
        $subject = new ShopTransformer();
        $this->assertInstanceOf(TransformerAbstract::class, $subject);
    }

    /** @test **/
    public function it_transforms_a_shop_model()
    {
        $shop = factory(Shop::class)->create();
        $subject = new ShopTransformer();

        $transform = $subject->transform($shop);
        $this->assertArrayHasKey('uuid', $transform);
        $this->assertArrayHasKey('name', $transform);
        $this->assertArrayHasKey('email', $transform);
        $this->assertArrayHasKey('information', $transform);
        $this->assertArrayHasKey('phone', $transform);
        $this->assertArrayHasKey('picture', $transform);
        $this->assertArrayHasKey('created_at', $transform);
        $this->assertArrayhasKey('updated_at', $transform);
    }
}
