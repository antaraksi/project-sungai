<?php

namespace Tests\App\Http\Controllers;

use TestCase;
use Carbon\Carbon;
use Laravel\Lumen\Testing\DatabaseMigrations;

class ShopsControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        Carbon::setTestNow(Carbon::now('Asia/Jakarta'));
    }

    public function tearDown()
    {
        parent::tearDown();
        Carbon::setTestNow();
    }

    /** @test **/
    public function index_status_code_should_be_200()
    {
        $this->get('/api/v2/shops')->seeStatusCode(200);
    }

    /** @test **/
    public function index_should_return_collection_of_records()
    {
        $shops = $this->shopFactory(2);

        $this->get('/api/v2/shops');

        $content = json_decode($this->response->getContent(), true);
        $this->assertArrayHasKey('data', $content);

        foreach ($shops as $shop) {
            $this->seeJson([
                'uuid' => $shop->uuid,
                'name' => $shop->name,
                'email' => $shop->email,
                'information' => $shop->information,
                'picture' => $shop->picture,
                'created_at' => $shop->created_at->toIso8601String(),
                'updated_at' => $shop->updated_at->toIso8601String()
            ]);
        }
    }

    /** @test **/
    public function show_should_return_a_valid_shop()
    {
        $shops = $this->shopFactory();

        foreach ($shops as $shop) {
            $this
                ->get("/api/v2/shops/{$shop->uuid}")
                ->seeStatusCode(200);

            // Get the response and assert the data key exists
            $content = json_decode($this->response->getContent(), true);
            $this->assertArrayHasKey('data', $content);

            $data = $content['data'];

            // Assert the shop properties match
            $this->assertEquals($shop->uuid, $data['uuid']);
            $this->assertEquals($shop->name, $data['name']);
            $this->assertEquals($shop->email, $data['email']);
            $this->assertEquals($shop->information, $data['information']);
            $this->assertEquals($shop->phone, $data['phone']);
            $this->assertEquals($shop->created_at->toIso8601String(), $data['created_at']);
            $this->assertEquals($shop->updated_at->toIso8601String(), $data['updated_at']);
        }
    }

    /** @test **/
    public function show_should_send_empty_when_the_shop_id_does_not_exist()
    {
        $this
            ->get('/api/v2/shops/99999', ['Accept' => 'application/json'])
            ->seeStatusCode(200)
            ->seeJson(['data' => []]);
    }

}
