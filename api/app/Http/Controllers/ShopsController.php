<?php

namespace App\Http\Controllers;

use App\Shop;
use Illuminate\Http\Request;
use App\Transformer\ShopTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class ShopsController
 * @package App\Http\Controllers
 */
class ShopsController extends Controller
{
    /**
     * GET /shops
     * @return array
     */
    public function index()
    {
        return $this->collection(Shop::all(), new ShopTransformer());
    }

    /**
     * GET /shops/{id}
     * @param uuid $uuid
     * @return array
     */
    public function show($uuid)
    {
        $shop = Shop::where('uuid', $uuid)->first();

        if (is_null($shop)) {
            return $this->sendEmptyCollection();
        }

        return $this->item($shop, new ShopTransformer());
    }
}
