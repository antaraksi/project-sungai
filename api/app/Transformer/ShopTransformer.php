<?php

namespace App\Transformer;

use App\Shop;
use League\Fractal\TransformerAbstract;

class ShopTransformer extends TransformerAbstract
{
    /**
     * Transform a Shop model into array
     *
     * @param Shop $shop
     * @return array
     */
    public function transform(Shop $shop)
    {
        return [
            'uuid'          => $shop->uuid,
            'name'          => $shop->name,
            'email'         => $shop->email,
            'information'   => $shop->information,
            'phone'         => $shop->phone,
            'picture'       => $shop->picture,
            'created_at'    => $shop->created_at->toIso8601String(),
            'updated_at'    => $shop->updated_at->toIso8601String()
        ];
    }
}
